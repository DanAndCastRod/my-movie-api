from fastapi import APIRouter
from fastapi import Depends,   Path, Query, Request, HTTPException
from fastapi.responses import  JSONResponse
from pydantic import BaseModel, Field
from typing import Optional, List
from utils.jwt_manager import  validate_token
from fastapi.security import HTTPBearer
from config.database import Session
from models.movie import Movie as MovieModel
from fastapi.encoders import jsonable_encoder
from services.movie import MovieService
from schemas.movie import Movie

movie_router = APIRouter()





class JWTBearer(HTTPBearer):
    async def __call__(self, request: Request):
        auth = await super().__call__(request)
        data = validate_token(auth.credentials)
        if data['email'] != "admin@gmail.com":
            raise HTTPException(status_code = 403, detail = "Sus credenciales son invalidas")

@movie_router.get('/movies', tags = ['movies'],response_model = List[Movie],status_code = 200)#, dependencies = [Depends(JWTBearer())])
def get_movies() -> List[Movie]:
    db = Session()
    result = MovieService(db).get_movies()
    return JSONResponse(status_code=200,content = jsonable_encoder(result))

@movie_router.get('/movies/{id}', tags = ['movies'], response_model = Movie,status_code=404)
def get_movie(id: int = Path(ge = 1, le = 2000)) -> Movie:
    db = Session()
    result = MovieService(db).get_movie(id)
    if not result:
        return JSONResponse(status_code = 404, content = {'message':'No encontrado'})
    return JSONResponse(status_code=200, content = jsonable_encoder(result))


@movie_router.get('/movies/', tags= ['movies'], response_model = List[Movie])
def get_movies_by_category(category: str = Query(min_length=5, max_length=50)) -> List[Movie]:
    db = Session()
    result = MovieService(db).get_movie_by_category(category)
    

    return JSONResponse(content = jsonable_encoder(result))

# Método Post

@movie_router.post('/movies', tags = ['movies'], response_model = dict, status_code = 201)
def create_movie(movie:Movie)->dict:

    db = Session()
    MovieService(db).create_movie(movie)

    return JSONResponse(status_code=201, content = {"message":"Se ha registrado la Película"})

# Modificaciones

@movie_router.put('/movies/{id}', tags=['movies'],response_model = dict,status_code=200)
def update_movie(id: int,movie: Movie)->dict:
    db = Session()
    result = MovieService(db).get_movie(id)
    if not result:
        return JSONResponse(status_code = 404, content = {'message':'No encontrado'})
    MovieService(db).update_movie(id, movie)
    return JSONResponse(status_code=200,content={"message":"Se ha modificado la película"})

@movie_router.delete('/movies/{id}', tags = ['movies'],response_model = dict,status_code=200)
def delete_movie(id:int)->dict:
    db = Session()
    result: MovieModel = db.query(MovieModel).filter(MovieModel.id == id).first()
    if not result:
        return JSONResponse(status_code=404, content = {'message':'No encontrado'})
    MovieService(db).delete_movie(id)
    
    return JSONResponse(status_code=200,content = {"message": "Se ha eliminado la película"})
from pydantic import BaseModel, Field
from typing import Optional

class Movie(BaseModel):
    id:Optional[int] = None
    title:str = Field(max_length=50)
    overview: str
    year: int = Field( le = 2023, ge = 2018)
    rating: float
    category: str

    class Config:
        schema_extra ={
            "example" :{
                "id":1,
                "title": "Nueva pelicula",
                "overview": "Descripción de la película",
                "year": 2022,
                "rating": 5.0,
                "category": "Acción"
            }
        }

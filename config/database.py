import os
# import urllib
from sqlalchemy import create_engine
from sqlalchemy.orm.session import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

sqlit_file_name="../database.sqlite"
base_dir = os.path.dirname(os.path.realpath(__file__))

database_url = f"sqlite:///{os.path.join(base_dir, sqlit_file_name)}"
# server = 'localhost' # to specify an alternate port
# database = 'Apps'
# username = 'sa'
# password = '@Acceso23'
# params = urllib.parse.quote_plus("'DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password")

# engine = create_engine("mssql+pyodbc:///?odbc_connect=%s" % params)
engine = create_engine(database_url, echo = True)

Session = sessionmaker(bind = engine)

Base = declarative_base()
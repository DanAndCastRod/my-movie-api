from fastapi import FastAPI, Request
from fastapi.responses import HTMLResponse, JSONResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates

from pydantic import BaseModel

from utils.jwt_manager import create_token
from config.database import  engine, Base
from routers.movie import movie_router
from routers.users import user_router

# Pydantic se usa para modelos de datos.
# Tanto Query como Path de la libreria FastApi Se utilizar para las validaciones.

app = FastAPI()
app.title = "Mi aplicacion con FASTAPI"
app.version = "0.0.1"

app.include_router(movie_router)
#app.include_router(user_router)

Base.metadata.create_all(bind = engine)

app.mount("/static", StaticFiles(directory="static"), name = "static")

templates = Jinja2Templates(directory = "templates")

@app.get("/", response_class = HTMLResponse)
async def home(request: Request):
    return templates.TemplateResponse("index.html", {
        "request":request,
        "message": "Primera impresion con HTML"
    })